﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class FemalePlayerController : MonoBehaviourPun
{
    private bool crouchBool=false;
    private bool blockBool=false;
    private bool dead=false;
    private bool InAir; 
    private bool Left;
    private bool Right;
    private bool Stay=true;
    public static bool _CanAttack = false;
    
    public Animator _animator;
    
    private PhotonView PV;

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
    }

    void Start()
    {
        Left = true;
        Right = false;
    }

    IEnumerator COInAir(float toAnimWindow)
    {
        Stay = false;
        yield return new WaitForSeconds(toAnimWindow);
        InAir = true;
        _animator.SetBool("InAir", true);
        yield return new WaitForSeconds(0.5f);
        Stay = true;
        InAir = false;
        _animator.SetBool("InAir", false);
    }
    public IEnumerator NormalAttack()
    {
        _CanAttack = true;
        _animator.SetTrigger("PunchTrigger");
        yield return new WaitForSeconds(0.3f);
        _CanAttack = false;
    }
    public void Attacked()
    {
        _animator.SetTrigger("KnockdownTrigger");
    }
    void Update()
    {
        if (!PV.IsMine)
        {
         return;
        }
        if (!dead)
        {
            if(Input.GetKeyUp(KeyCode.A)||Input.GetKeyUp(KeyCode.D))
            {
                _animator.SetBool("Walk Forward",false);  
            } 
            if (Input.GetKey(KeyCode.A)&&Left==true)
            {
                _animator.SetBool("Walk Forward", true);
            }
            if(Input.GetKey(KeyCode.A)&&Left == false)
            {
                Left = true;
                Right = false;
                transform.Rotate(0,180,0);
                _animator.SetBool("Walk Forward",true);
            }
            if (Input.GetKey(KeyCode.D)&&Right==false)
            {
                Right = true;
                Left = false;
                transform.Rotate(0,180,0);
                _animator.SetBool("Walk Forward",true);
            }
            if (Input.GetKey(KeyCode.D)&&Right==true)
            {
                _animator.SetBool("Walk Forward",true);
            }

            if (Input.GetKey(KeyCode.S))
            {
                _animator.SetBool("Crouch",true);
            }
            else
            {
                _animator.SetBool("Crouch",false);
            }
            if (Input.GetKey(KeyCode.Q))
            {
                _animator.SetBool("Block",true);
            }
            else
            {
            
                _animator.SetBool("Block",false);
            }
            if (Input.GetKeyDown(KeyCode.Space)&&Input.GetKey(KeyCode.A)
                                               &&!InAir&&Stay==true)
            {
                _animator.SetTrigger("JumpForwardTrigger");
                StartCoroutine (COInAir(0.25f));
            }
            if (Input.GetKeyDown(KeyCode.Space)&&Input.GetKey(KeyCode.D)
                                               &&!InAir&&Stay==true)
            {
                _animator.SetTrigger("JumpForwardTrigger");
                StartCoroutine (COInAir(0.25f)); 
            } 
            if (Input.GetKeyDown(KeyCode.Space)&&Stay==true)
            {
                _animator.SetTrigger("JumpTrigger");
                StartCoroutine (COInAir(0.25f));
            }

            if (Input.GetKeyDown(KeyCode.K))
            {
                StartCoroutine(NormalAttack());
            }
            if (Input.GetKeyDown(KeyCode.K)&&InAir==true)
            {
               _animator.SetTrigger("HighSmashTrigger"); 
            }
            if (Input.GetKeyDown(KeyCode.L))
            {
                _animator.SetTrigger("HeavySmashTrigger");
            }

            if (Input.GetKey(KeyCode.S)&&Input.GetKeyDown(KeyCode.K))
            {
                _animator.SetTrigger("DownSmashTrigger");
            }
        }

        if (dead)
        {
            _animator.SetTrigger("ReviveTrigger");
            dead = false;
        }
    }


}
