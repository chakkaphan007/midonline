﻿using System;
using Photon.Pun;
using UnityEngine;

public class PunHitCheck : MonoBehaviourPun,IPunInstantiateMagicCallback
{
    public int damage = 10;
    int OwnerViewID = -1;
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        info.Sender.TagObject = this.gameObject;
        OwnerViewID = info.photonView.ViewID;
        if (!photonView.IsMine)
            return;

    }
    private void OnTriggerEnter(Collider other)
    {
        if (!photonView.IsMine)
            return;
        if (other.gameObject.tag == "Player"&& FemalePlayerController._CanAttack==true)
        {
                Debug.Log("Damage");
                photonView.RPC("Damage",RpcTarget.AllBufferedViaServer);
        } 
    }
}
