﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class BillBoard : MonoBehaviour
{
    public Transform MainCamera;
    void Start()
    {
        //MainCamera = Camera.main.transform;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.LookAt(transform.position + MainCamera.rotation
        * Vector3.forward,MainCamera.rotation*Vector3.up);
    }
}
