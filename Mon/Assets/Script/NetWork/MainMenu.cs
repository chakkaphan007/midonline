﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;

public class MainMenu : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject findOpponentPanel = null;
    [SerializeField] private GameObject WaittingStatusPanel = null;
    [SerializeField] private TextMeshProUGUI waittingStatusText = null;
    
    private bool isConnecting = false;
    private const string Gameversion = "1";
    private const int MaxPlayerPerRoom = 2;

    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    public void FindOpponent()
    {
        isConnecting = true;
        
        findOpponentPanel.SetActive(false);
        WaittingStatusPanel.SetActive(true);

        waittingStatusText.text = "loading";
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.JoinRandomRoom();
        }
        else
        {
            PhotonNetwork.GameVersion = Gameversion;
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    public override void OnConnectedToMaster()
    {
        if (isConnecting)
        {
            PhotonNetwork.JoinRandomRoom();
        }
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        WaittingStatusPanel.SetActive(false);
        findOpponentPanel.SetActive(true);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("JoinRoomFail");
        PhotonNetwork.CreateRoom(null, new RoomOptions{MaxPlayers = MaxPlayerPerRoom});
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("JoinedRoom");
        int PlayerCount = PhotonNetwork.CurrentRoom.PlayerCount;

        if (PlayerCount!=MaxPlayerPerRoom)
        {
            waittingStatusText.text = "Waitting For Opponent";
        }
        else
        {
            waittingStatusText.text = "Opponent Found";
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        if (PhotonNetwork.CurrentRoom.PlayerCount == MaxPlayerPerRoom)
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;
            waittingStatusText.text = "Opponent Found";
            PhotonNetwork.LoadLevel("Main");
        }
    }
}
