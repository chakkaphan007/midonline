﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class HealthSync : MonoBehaviourPun
{
    [SerializeField] private Slider _slider;
    void Start()
    {
        if (photonView.IsMine)
        {
            return;
        }
        setHealth();
    }

    private void setHealth()
    {
    }
}
