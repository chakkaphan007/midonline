﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider _Slider;

    public void SetHealth(int Health)
    {
        _Slider.value = Health;
    }

    public void SetMaxHealth(int Health)
    {
        _Slider.maxValue = Health;
        _Slider.value = Health;
    }
}
