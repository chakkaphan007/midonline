﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NameInput : MonoBehaviour
{
    public TMP_InputField nameInputField = null;

    public Button NextButton = null;
    private const string PlayerPrefsNameKey = "PlayerName";

    private void Start() => SetupInputField();

    private void SetupInputField()
    {
        if (!PlayerPrefs.HasKey(PlayerPrefsNameKey))
        {
            return;
        }

        string defaultName = PlayerPrefs.GetString(PlayerPrefsNameKey);
        nameInputField.text = defaultName;
        SetPlayerName(defaultName);
    }

    public void SetPlayerName(string name)
    {
        NextButton.interactable = !string.IsNullOrEmpty(name);
    }

    public void SavePlayerName()
    {
        string PlayerName = nameInputField.text;
        PhotonNetwork.NickName = PlayerName;
        PlayerPrefs.SetString(PlayerPrefsNameKey,PlayerName);
    }

}
