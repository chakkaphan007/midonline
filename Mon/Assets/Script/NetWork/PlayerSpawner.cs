﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Cinemachine;
using UnityEngine;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using UnityEngine.SceneManagement;

public class PlayerSpawner : MonoBehaviour 
{
    public GameObject P1;
    public GameObject P2;
    public GameObject P3;
    public GameObject P4;

    
    public static bool player1 = false;
    public static bool player2 = false;
    public static bool player3 = false;
    public static bool player4 = false;
    [SerializeField] private CinemachineVirtualCamera PlayerCamera;
    private void Start()
    {
        SpawnPlayer();
    }
    public void SpawnPlayer()
    {
        if (player1==true)
        {
            var player = PhotonNetwork.Instantiate(P1.name, new Vector3
                (0f, 3f, 0f), Quaternion.identity, 0);
            PlayerCamera.LookAt = player.transform;
            player1 = false;
        }
        if (player2==true)
        {
            var player = PhotonNetwork.Instantiate(P2.name, new Vector3
                (0f, 3f, 0f), Quaternion.identity, 0);
            PlayerCamera.LookAt = player.transform;
            player2 = false;
        }
        if (player3==true)
        {
            var player = PhotonNetwork.Instantiate(P3.name, new Vector3
                (0f, 3f, 0f), Quaternion.identity, 0);
            PlayerCamera.LookAt = player.transform;
            player3 = false;
        }
        if (player4==true)
        {
            var player = PhotonNetwork.Instantiate(P4.name, new Vector3
                (0f, 3f, 0f), Quaternion.identity, 0);
            PlayerCamera.LookAt = player.transform;
            player4 = false;
        }
        
    }
}
