﻿using System;
using Photon.Pun;
using Photon.Pun.Demo.Procedural;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class PhotonHealth : MonoBehaviourPunCallbacks
{
    public const int maxHealth = 100;
    public int currentHealth = maxHealth;
    public Animator _Animator;
    public HealthBar _HealthBar;
    
    [SerializeField] private TextMeshProUGUI _health;

    private void Awake()
    {
        if (photonView.IsMine)
        {
            return;
        }  
    }

    private void FixedUpdate()
    {        
            _health.text = currentHealth.ToString();
            if (currentHealth<=0)
            {
                if (photonView.IsMine)
                {
                    PhotonNetwork.Disconnect(); 
                }
                
            }
    }
    /*private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player"&& FemalePlayerController._CanAttack==true)
        {
            if (photonView.IsMine)
            {
                Debug.Log("Damage");
                photonView.RPC("Damage",RpcTarget.AllBufferedViaServer);
            }
        } 
    }*/

    [PunRPC]
    void Damage()
    {
        currentHealth -= 20;
    }
    
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(currentHealth);
        }
        else if (stream.IsReading)
        {
            currentHealth = (int)stream.ReceiveNext();
        }
    }
}
