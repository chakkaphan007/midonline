﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectChar : MonoBehaviour
{
    public void SelectP1()
    {
        PlayerSpawner.player1 = true;
    }
    public void SelectP2()
    {
        PlayerSpawner.player2 = true;
    }
    public void SelectP3()
    {
        PlayerSpawner.player3 = true;
    }
    public void SelectP4()
    {
        PlayerSpawner.player4 = true;
    }
}
