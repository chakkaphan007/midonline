﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerTag : MonoBehaviourPun
{
    [SerializeField] private TextMeshProUGUI name;
    private void Start()
    {
        if (photonView.IsMine)
        {
            return;
        }
        setname();
    }

    private void setname()
    {
        name.text = photonView.Owner.NickName;
    }
}
