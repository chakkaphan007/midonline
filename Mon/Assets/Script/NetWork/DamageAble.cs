﻿public interface DamageAble
{
    void TakeDamage(float damage);
}
