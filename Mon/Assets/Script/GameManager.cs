﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using UnityEngine;

public class GameManager :ConnectAndJoinRandom 
{
    public GameObject playerPrefab;
    public static GameManager singleton;
    
    public delegate void PlayerSpawned();
    public static event PlayerSpawned OnPlayerSpawned;
    private void Awake()
    {
       singleton = this;
       
       OnPlayerSpawned += SpawnPlayer;
    }

    void Start()
    {
        if (playerPrefab == null)
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> playerPrefab Reference. Please set it up in GameObject 'Game Manager'",this);
        }
        else
        {
            Debug.LogFormat("We are Instantiating LocalPlayer from {0}", Application.loadedLevelName);
            // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
            PhotonNetwork.Instantiate(this.playerPrefab.name, new Vector3(0f,0f,0f), Quaternion.identity, 0); 
        }
    }
    public override void OnPlayerEnteredRoom(Player newPlayer) {
        base.OnPlayerEnteredRoom(newPlayer);

        Debug.Log("New Player. " + newPlayer.ToString());
    }
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Camera.main.gameObject.SetActive(false);

        //Delegate Function Call when Player Joined to Room.
        OnPlayerSpawned();
    }
   public void SpawnPlayer()
    {
         
    }
}
